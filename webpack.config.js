const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [{
		mode: 'development',

    entry: {
        lift: "./src/app/rm/index.tsx"
		},

    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "dist/lift"),
        // libraryTarget: 'amd',
        publicPath: '/lift/'
		},

		plugins: [
			new HtmlWebpackPlugin({
				title: 'Реклама в лифтах',
				template: './index.html',
				filename: '../lift-landing.html'
			}),
		],

    devtool: "source-map",

    resolve: {
        modules: ['node_modules', 'src'],
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"]
    },

    module: {
        rules: [
            { test: /\.tsx?$/, use: "awesome-typescript-loader" },
            { test: /\.(jpe?g|gif|png|svg|woff2?|ttf|eot|wav|mp3|webp)$/, use: "file-loader" }
        ],

		},
},];