export const ICONS = Object.freeze({
  Facebook: {symbol: 'e904', name: 'Facebook', href: '#Facebook'},
  GooglePlus: {symbol: 'e903', name: 'GooglePlus', href: '#GooglePlus'},
  Instagram: {symbol: 'e902', name: 'Instagram', href: '#Instagram'},
  Linkedin: {symbol: 'e901', name: 'Linkedin', href: '#Linkedin'},
  Odnoklassniki: {symbol: 'e900', name: 'Odnoklassniki', href: '#Odnoklassniki'},
  Twitter: {symbol: 'e906', name: 'Twitter', href: '#Twitter'},
  Vk: {symbol: 'e907', name: 'Vk', href: '#Vk'},
  YouTube: {symbol: 'e905', name: 'YouTube', href: '#YouTube'},
  ArrowDown: {symbol: 'e908', name: 'ArrowDown', href: '#ArrowDown'},
  Messages: {symbol: 'e909', name: 'Messages', href: '#Messages'},
  Purchases: {symbol: 'e90a', name: 'Purchases', href: '#Purchases'}
});

export const WIDTH = Object.freeze({
  PERCENTS: {
    LEVEL1: 98,
    LEVEL2: 75
  },
  PIXELS: {
    LEVEL1: 1360,
    LEVEL2: 1170,
    LEVEL3: 900
  }
});

export const COLORS = Object.freeze({
  BLUE2: '#2ea2f8',
  BLUE: '#00aeef',
  GRAY: '#354052',
  ORANGE: '#f36a19',
  WARM_WHITE: '#eff3f6',
  WHITE2: '#dfe3e9',
  WHITE3: '#f1f4f8',
  WHITE: '#fff'
});

export const LINEAR_GRADIENTS = Object.freeze({
  YELLOW: 'linear-gradient(90deg, #f76d1c 0%, #f7971c 100%)',
  BLUE: 'linear-gradient(to right, #1991eb, #00aeef)'
});

export const FONTS_STACK = Object.freeze({
  PROXIMA_NOVA: '\'Proxima Nova\', Georgia, sans-serif'
});
