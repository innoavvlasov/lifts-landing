import styled from 'styled-components';

export const StyledDelimiter = styled.div`
  border-top: 1px solid #e7e9ed;
`;
