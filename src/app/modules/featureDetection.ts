export function isWebpSupported(): boolean {
  return (window as any).Modernizr && (window as any).Modernizr.webp.valueOf();
}
