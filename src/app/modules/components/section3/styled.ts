import styled from 'styled-components';
import { COLORS, FONTS_STACK, WIDTH } from '../../constants';

const StyledText = styled.div`
  width: 560px;
`;

export const StyledSection3H1 = StyledText.extend`
  padding-top: 19px;
  font: normal 400 32px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
  color: ${COLORS.BLUE};
`.withComponent('h1');

export const StyledSection3P = StyledText.extend`
  margin: 0;
  padding-top: 5px;
  font: normal 400 18px/1.662 ${FONTS_STACK.PROXIMA_NOVA};
  color: ${COLORS.GRAY};
`.withComponent('p');

/* https://github.com/styled-components/stylelint-processor-styled-components/issues/54 */
export const StyledSection3Container = styled.section`
  box-sizing: border-box;
  margin: 96px auto 113px;
  width: ${WIDTH.PIXELS.LEVEL3}px;
  background: url(${require('./image.svg')}) no-repeat left center/220px 191px;
  background-position-x: 27px;
  background-position-y: 13px;
  padding: 0 0 13px 286px;

  @media only screen and (max-width: 1000px) {
    width: ${WIDTH.PERCENTS.LEVEL2}%;
    text-align: center;
    padding: 180px 0 0;
    background-position: top center;

    /* stylelint-disable declaration-empty-line-before, declaration-block-semicolon-newline-after */
    /* stylelint-disable rule-empty-line-before, selector-type-case, selector-type-no-unknown */
    ${/* sc-selector */ StyledSection3H1},
    ${/* sc-selector */ StyledSection3P} {
      width: 100%;
    }
    /* stylelint-enable */
  }
`;
