import * as React from 'react';
import { StyledSection3Container, StyledSection3H1, StyledSection3P } from './styled';

export default class Section3 extends React.PureComponent<{}, {}> {

  public render(): React.ReactNode {
    return (
      <StyledSection3Container>
        <StyledSection3H1>
          Стоимость фиксирована
        </StyledSection3H1>
        <StyledSection3P>
          Mediapult не берет с рекламодателей комиссию за пользование системой. Система гарантирует, что стоимость,
          отражаемая на сайте MediaPult равна стоимости, которую заявляет владелец рекламной площади в лифтах.
        </StyledSection3P>
      </StyledSection3Container>
    );
  }

}
