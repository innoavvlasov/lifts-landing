import styled from 'styled-components';
import { COLORS, FONTS_STACK, WIDTH } from '../../constants';
import { StyledTryNowButton } from '../controls/buttons/try-now-button/styled';

/* https://github.com/styled-components/stylelint-processor-styled-components/issues/54 */
export const StyledSection4Container = styled.section`
  padding: 75px 0 79px;
  text-align: center;
  width: 100%;
  background-color: ${COLORS.WARM_WHITE};

  /* stylelint-disable declaration-empty-line-before, declaration-block-semicolon-newline-after */
  /* stylelint-disable rule-empty-line-before, no-duplicate-selectors */
  ${/* sc-selector */ StyledTryNowButton} {
    margin-top: 42px;
  }
  /* stylelint-enable */
`;

export const StyledSection4H1 = styled.h1`
  width: ${WIDTH.PERCENTS.LEVEL1}%;
  margin: 0 auto;
  text-align: center;
  max-width: ${WIDTH.PIXELS.LEVEL3}px;
  font: normal 300 33px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
  color: ${COLORS.GRAY};
`;
