import * as React from 'react';
import { StyledSection4Container, StyledSection4H1 } from './styled';
import { SendStateEventProps } from '../../index';
import TryNowButton from '../controls/buttons/try-now-button';

export default class Section4 extends React.PureComponent<SendStateEventProps, {}> {

  public render(): React.ReactNode {
    return (
      <StyledSection4Container>
        <StyledSection4H1>Запустить рекламу в лифтах вашего города!</StyledSection4H1>
        <TryNowButton {...this.props}/>
      </StyledSection4Container>
    );
  }

}
