export interface ClientInfoResponse {

  fname: string;
  lname: string;
  messages: string;
  purchases: string;
  userphotourl: string;

}

export function isClientInfoResponse(json: any): json is ClientInfoResponse {
  const cast: ClientInfoResponse = json as ClientInfoResponse;
  return cast.fname !== undefined && cast.lname !== undefined && cast.messages !== undefined
    && cast.purchases !== undefined && cast.userphotourl !== undefined;
}

export function retrieveClientInfo(): Promise<ClientInfoResponse> {
  return fetch('/getClient')
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        console.error('Network response was not ok');
        throw new Error();
      }
    })
    .then(json => {
      if (isClientInfoResponse(json)) {
        return json;
      } else {
        console.error('Client info is not properly formatted');
        throw new Error();
      }
    })
    .catch(error => {
      throw new Error('There has been a problem with your fetch operation: ' + error.message);
    });
}
