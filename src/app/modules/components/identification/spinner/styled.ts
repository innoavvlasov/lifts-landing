import styled, { keyframes } from 'styled-components';

const animation = keyframes`
  0% {
    transform: rotate(0deg);
  }

  100% {
    transform: rotate(360deg);
  }
`;

export const StyledSpinner = styled.div`
  display: inline-block;
  margin: 0 10px;
  width: 36px;
  height: 36px;
  position: relative;

  &::before {
    position: absolute;
    top: 12px;
    content: '';
    border-radius: 50%;
    box-sizing: border-box;
    width: 100%;
    height: 100%;
    border: 3px solid rgba(255, 255, 255, 0.2);
    border-left-color: #fff;
    animation: ${animation} 1.1s infinite linear;
  }
`;
