import * as React from 'react';
import PromiseDrivenIdentification from './promise-driven';
import { SignedIdentificationData } from './promise-driven/signed';
import { retrieveClientInfo } from './api';

interface IdentificationProps {
  action: () => void;
}

interface IdentificationState {
  signedPropsPromise: Promise<SignedIdentificationData>;
}

export default class Identification extends React.PureComponent<IdentificationProps, IdentificationState> {

  public constructor(props: IdentificationProps) {
    super(props);
    this.state = {
      signedPropsPromise: retrieveClientInfo()
        .then(response => ({
          messages: parseInt(response.messages, 10),
          purchases: parseInt(response.purchases, 10),
          imageUrl: response.userphotourl
        }))
    };
  }

  public render(): React.ReactNode {
    return (
      <PromiseDrivenIdentification
        action={this.props.action}
        signedPropsPromise={this.state.signedPropsPromise}
      />
    );
  }

}
