import styled from 'styled-components';
import RippleButton from '../../../controls/buttons/ripple-button';
import { COLORS } from '../../../../constants';
import { styledAsceticControlCss } from '../../../controls/styled';

export const StyledButton = styled.button`
  margin-right: 14px;
  ${styledAsceticControlCss};
`;

export const RegistrationButton = styled(RippleButton)`
  background-color: transparent;
  border: 1px solid ${COLORS.WHITE};
  border-radius: 4px;
  margin-right: 4px;
  padding: 10px 14px;

  &:hover,
  &:focus,
  &:active {
    box-shadow: none;
    border-color: ${COLORS.GRAY};
  }
`;
