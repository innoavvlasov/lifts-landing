import * as React from 'react';
import { StyledSpinner } from '../../spinner/styled';
import { StyledActions } from '../styled';
import { RegistrationButton, StyledButton } from './styled';

interface NonSignedIdentificationProps {
  action: () => void;
  spinner: boolean;
}

export default class NonSignedIdentification extends React.PureComponent<NonSignedIdentificationProps, {}> {

  public render(): React.ReactNode {
    return (
      <StyledActions>
        <StyledButton
          onClick={() => this.props.action()}
          type="button"
        >
          Войти
        </StyledButton>
        <RegistrationButton
          action={this.props.action}
          text="Регистрация"
        />
        {this.props.spinner && <StyledSpinner/>}
      </StyledActions>
    );
  }

}
