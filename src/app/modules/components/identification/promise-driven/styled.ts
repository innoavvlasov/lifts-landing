import styled from 'styled-components';
import { FONTS_STACK } from '../../../constants';

export const StyledActions = styled.nav`
  display: inline-block;
  font: normal 700 14px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
  position: relative;

  @media only screen and (max-width: 700px) {
    margin-left: auto;
  }
`;
