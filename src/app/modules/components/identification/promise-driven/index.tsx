import * as React from 'react';
import { default as SignedIdentification, SignedIdentificationData } from './signed';
import NonSignedIdentification from './non-signed';

interface IdentificationProps {
  action: () => void;
  signedPropsPromise: Promise<SignedIdentificationData>;
}

interface IdentificationState {
  signedProps: SignedIdentificationData | null;
  spinner: boolean;
}

export default class Identification extends React.PureComponent<IdentificationProps, IdentificationState> {

  public constructor(props: IdentificationProps) {
    super(props);
    this.state = {signedProps: null, spinner: true};

    props.signedPropsPromise
      .then(signedProps => this.setState({signedProps}))
      .catch(() => this.setState({spinner: false}));
  }

  public render(): React.ReactNode {
    return (
      this.state.signedProps ?
        <SignedIdentification {...this.state.signedProps} action={this.props.action}/> :
        <NonSignedIdentification action={this.props.action} spinner={this.state.spinner}/>
    );
  }

}
