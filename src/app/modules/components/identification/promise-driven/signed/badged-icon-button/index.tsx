import * as React from 'react';
import { StyledBadgedIconButton } from './styled';

interface BadgedIconButtonProps {
  iconSymbol: string;
  badge: string;
  description: string;
  action: () => void;
  className?: string;
}

export class BadgedIconButton extends React.PureComponent<BadgedIconButtonProps, {}> {

  public render(): React.ReactNode {
    return (
      <StyledBadgedIconButton
        onClick={() => this.props.action()}
        type={'button'}
        title={this.props.description}
        aria-label={this.props.description}
        iconSymbol={this.props.iconSymbol}
        badge={this.props.badge}
        className={this.props.className}
      />
    );
  }

}
