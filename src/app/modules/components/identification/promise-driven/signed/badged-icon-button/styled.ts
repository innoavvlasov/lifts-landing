import styled from 'styled-components';
import { COLORS, FONTS_STACK } from '../../../../../constants';

interface StyledBadgedIconButtonProps {
  iconSymbol: string;
  badge: string;
}

export const StyledBadgedIconButton = styled<StyledBadgedIconButtonProps, 'button'>('button')`
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  outline: 0;
  border-radius: 5px;
  cursor: pointer;
  transition: color 0.1s ease-in-out;
  color: ${COLORS.WHITE};
  padding: 12px 14px 0 6px;
  border: 0;
  background-color: transparent;
  position: relative;

  &:hover,
  &:focus,
  &:active {
    color: ${COLORS.GRAY};
  }

  &::before {
    content: ${props => props.iconSymbol};
    /* stylelint-disable-next-line font-family-no-missing-generic-family-keyword */
    font: normal normal 16px icomoon;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    speak: none;
  }

  &::after {
    position: absolute;
    content: "${props => props.badge}";
    font: normal 600 12px/1 ${FONTS_STACK.PROXIMA_NOVA};
    background-color: #ff7800;
    transform: translate3d(-10px, -7px, 0);
    border-radius: 50%;
    padding: 2px 5px;
    box-sizing: border-box;
  }
`;
