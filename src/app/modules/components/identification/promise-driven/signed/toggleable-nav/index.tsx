import * as React from 'react';
import { StyledButton, StyledLi, StyledToggleButton, StyledUl } from './styled';

interface ToggleableNavProps {
  description: string;
  imageUrl: string;
  action: () => void;
}

interface ToggleableNavState {
  expand: boolean;
}

export default class ToggleableNav extends React.PureComponent<ToggleableNavProps, ToggleableNavState> {

  private list: HTMLUListElement;

  public constructor(props: ToggleableNavProps) {
    super(props);
    this.state = {expand: false};
  }

  public render(): React.ReactNode {
    return (
      <React.Fragment>
        <StyledToggleButton
          imageUrl={this.props.imageUrl}
          onClick={() => this.setState((prevState, props) => ({expand: !prevState.expand}))}
          type={'button'}
          aria-label={this.props.description}
          onBlur={(event) => {
            const relatedNode: HTMLElement = event.relatedTarget as HTMLElement;
            if (this.list.contains(relatedNode)) {
              relatedNode.click();
            }
            this.setState({expand: false});
          }}
        />
        <StyledUl
          visible={this.state.expand}
          innerRef={(list) => this.list = list}
        >
          <StyledLi bordered={false}>
            <StyledButton type={'button'} onClick={() => this.props.action()}>Личный кабинет</StyledButton>
          </StyledLi>
          <StyledLi bordered={true}>
            <StyledButton type={'button'} onClick={() => this.props.action()}>Комментарии</StyledButton>
          </StyledLi>
          <StyledLi bordered={false}>
            <StyledButton type={'button'} onClick={() => this.props.action()}>Выйти</StyledButton>
          </StyledLi>
        </StyledUl>
      </React.Fragment>
    );
  }

}
