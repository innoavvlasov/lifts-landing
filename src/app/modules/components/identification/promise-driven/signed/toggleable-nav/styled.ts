import styled, { css } from 'styled-components';
import { COLORS, FONTS_STACK, ICONS } from '../../../../../constants';
import { visuallyHidden } from '../../../../styled';

export const StyledButton = styled.button`
  padding: 6px 10px;
  width: 100%;
  text-align: left;
  font: normal 600 14px/2.143 ${FONTS_STACK.PROXIMA_NOVA};
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  outline: 0;
  border-radius: 5px;
  cursor: pointer;
  transition: color 0.1s ease-in-out;
  color: ${COLORS.GRAY};
  border: 0;
  background-color: transparent;

  &:hover,
  &:focus,
  &:active {
    color: ${COLORS.BLUE2};
    background-color: ${COLORS.WHITE3};
  }
`;

interface StyledUlProps {
  visible: boolean;
}

export const StyledUl = styled<StyledUlProps, 'ul'>('ul')`
  position: absolute;
  top: 50px;
  right: 4px;
  ${props => props.visible ? '' : visuallyHidden};
  list-style: none;
  background-color: ${COLORS.WHITE};
  margin: 0;
  padding: 0;
  width: 143px;
  border: 1px solid ${COLORS.WHITE2};
  border-radius: 4px;
`;

interface StyledLiProps {
  bordered: boolean;
}

const bordered = css`
  border-top: 1px solid ${COLORS.WHITE2};
  border-bottom: 1px solid ${COLORS.WHITE2};
`;

export const StyledLi = styled<StyledLiProps, 'li'>('li')`
  ${props => props.bordered ? bordered : ''};
`;

interface StyledToggleButtonProps {
  imageUrl: string;
}

export const StyledToggleButton = styled<StyledToggleButtonProps, 'button'>('button')`
  width: 60px;
  padding: 0 0 0 40px;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  outline: 0;
  border-radius: 5px;
  cursor: pointer;
  transition: box-shadow 0.1s ease-in-out, color 0.1s ease-in-out;
  background: url(${props => props.imageUrl}) no-repeat left top/36px 36px;
  height: 36px;
  color: ${COLORS.WHITE};
  border: 0;

  &:hover,
  &:focus,
  &:active {
    color: ${COLORS.GRAY};
  }

  &::after {
    content: "\\${ICONS.ArrowDown.symbol}";
    /* stylelint-disable-next-line font-family-no-missing-generic-family-keyword */
    font: normal normal 14px icomoon;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    speak: none;
  }
`;
