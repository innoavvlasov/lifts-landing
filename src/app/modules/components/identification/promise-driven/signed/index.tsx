import * as React from 'react';
import { StyledActions } from '../styled';
import { BadgedIconButton } from './badged-icon-button';
import { ICONS } from '../../../../constants';
import ToggleableNav from './toggleable-nav';

export interface SignedIdentificationData {
  messages: number;
  purchases: number;
  imageUrl: string;
}

export interface SignedIdentificationProps extends SignedIdentificationData {
  action: () => void;
}

export default class SignedIdentification extends React.PureComponent<SignedIdentificationProps, {}> {

  public render(): React.ReactNode {
    return (
      <StyledActions>
        <BadgedIconButton
          action={this.props.action}
          description={'Сообщения'}
          iconSymbol={`"\\${ICONS.Messages.symbol}"`}
          badge={this.props.messages.toString()}
        />
        <BadgedIconButton
          action={this.props.action}
          description={'Покупки'}
          iconSymbol={`"\\${ICONS.Purchases.symbol}"`}
          badge={this.props.purchases.toString()}
        />
        <ToggleableNav
          imageUrl={this.props.imageUrl}
          description={'Переключить список действий'}
          action={this.props.action}
        />
      </StyledActions>
    );
  }
}
