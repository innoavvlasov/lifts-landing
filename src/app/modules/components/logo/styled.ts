import styled from 'styled-components';
import { COLORS, FONTS_STACK } from '../../constants';

export const StyledLogoMedia = styled.span`
  margin-left: 4px;
  color: ${COLORS.WARM_WHITE};
`;

export const StyledLogoPult = styled.span`
  margin-left: 0.35em;
  color: ${COLORS.GRAY};
`;

export const StyledLogoAppendix = styled.span`
  display: block;
  margin: 0 0 0 4px;
  text-align: left;
  font: normal 600 10px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
  color: ${COLORS.WARM_WHITE};
`;

/* https://github.com/styled-components/stylelint-processor-styled-components/issues/54 */
export const StyledLogoContainer = styled.button`
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  outline: 0;
  border-radius: 5px;
  cursor: pointer;
  transition: box-shadow 0.1s ease-in-out, color 0.1s ease-in-out;
  border: 0;
  background-color: transparent;
  padding: 2px;
  font: normal 600 27px/1.125 ${FONTS_STACK.PROXIMA_NOVA};

  &:focus,
  &:active {
    box-shadow: 0 0 0 1px ${COLORS.GRAY};

    /* stylelint-disable rule-empty-line-before, declaration-block-semicolon-newline-after */
    /* stylelint-disable no-duplicate-selectors */
    ${/* sc-selector */ StyledLogoAppendix} {
      color: ${COLORS.GRAY};
    }
    /* stylelint-enable */
  }
`;
