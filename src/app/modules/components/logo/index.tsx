import * as React from 'react';
import { StyledLogoAppendix, StyledLogoContainer, StyledLogoMedia, StyledLogoPult } from './styled';
import { OpenAppProps } from '../../index';

export default class Logo extends React.PureComponent<OpenAppProps, any> {

  public render(): JSX.Element {
    return (
      <StyledLogoContainer onClick={() => this.props.openApp('defaultapp.bundle.js')}>
        <StyledLogoMedia>MEDIA</StyledLogoMedia>
        <StyledLogoPult>PULT</StyledLogoPult>
        <StyledLogoAppendix>Агрегатор оффлайн рекламы</StyledLogoAppendix>
      </StyledLogoContainer>
    );
  }

}
