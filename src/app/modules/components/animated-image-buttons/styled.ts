import styled from 'styled-components';

interface StyledAnimatedImageButtonsContainerProps {
  paddingTopInPercents: number;
  bottomOffset: number;
}

export const StyledAnimatedImageButtonsContainer = styled<StyledAnimatedImageButtonsContainerProps, 'figure'>('figure')`
  overflow-y: hidden;
  margin: 0 auto;
  padding-bottom: ${props => props.bottomOffset}px;
  width: 100%;
  position: relative;

  &::after {
    padding-top: ${props => props.paddingTopInPercents}%;
    display: block;
    content: '';
  }
`;

interface StyledAnimatedImageButtonProps {
  image: string;
  topOffset: string;
  imageWidthInPercents: number;
  translateInPercents: number;
  transparency: boolean;
}

const StyledAnimatedImageButton = styled<StyledAnimatedImageButtonProps, 'button'>('button')`
  border: 0;
  padding: 0;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  outline: 0;
  cursor: pointer;
  box-shadow: 0 6px 33.6px 1.4px rgba(0, 0, 0, 0.18);
  position: absolute;
  height: 100%;
  transition: transform 1s cubic-bezier(0, 0, 0, 1), opacity 0.25s cubic-bezier(0, 0, 0, 1);
  top: ${props => props.topOffset};
  width: ${props => props.imageWidthInPercents}%;
  background: url(${props => props.image}) no-repeat top left / 100% auto;
  transform: translate3d(${props => props.translateInPercents}%, 0, 0);
  opacity: ${props => +!props.transparency};

  &:hover {
    box-shadow: 0 6px 67.2px 2.8px rgba(0, 0, 0, 0.18);
  }
`;

export const StyledAnimatedLeftImageButton = StyledAnimatedImageButton.extend`
  left: 0;
  z-index: 1;
`;

export const StyledAnimatedRightImageButton = StyledAnimatedImageButton.extend`
  right: 0;
`;
