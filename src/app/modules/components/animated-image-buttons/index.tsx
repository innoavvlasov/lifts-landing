import * as React from 'react';
import {
  StyledAnimatedImageButtonsContainer,
  StyledAnimatedLeftImageButton,
  StyledAnimatedRightImageButton
} from './styled';

interface DefaultAnimatedImageButtonsProps {
  imageWidthInPercents: number;
  translateInPercents: number;
  leftImageBottomOffsetInPercents: number;
  rightImageTopOffsetInPercents: number;
  offset: number;
}

interface AnimatedImageButtonsProps extends Partial<DefaultAnimatedImageButtonsProps> {
  imageAspectRatio: number;
  leftImage: any;
  rightImage: any;
}

interface AnimatedImageButtonsState {
  translated: boolean;
}

export default class AnimatedImageButtons
  extends React.PureComponent<AnimatedImageButtonsProps, AnimatedImageButtonsState> {

  public static defaultProps: DefaultAnimatedImageButtonsProps = {
    imageWidthInPercents: 40,
    translateInPercents: 31.121,
    leftImageBottomOffsetInPercents: 6,
    rightImageTopOffsetInPercents: 30,
    offset: 50
  };

  private container: HTMLElement;

  private scrollListener: () => void = () => this.updateTranslated();

  public constructor(props: AnimatedImageButtonsProps) {
    super(props);
    this.state = {translated: false};
  }

  public componentDidMount(): void {
    window.addEventListener('scroll', this.scrollListener);
  }

  public componentWillUnmount(): void {
    window.removeEventListener('scroll', this.scrollListener);
  }

  public render(): React.ReactNode {
    const {
      imageAspectRatio,
      imageWidthInPercents,
      translateInPercents,
      leftImageBottomOffsetInPercents,
      rightImageTopOffsetInPercents,
      leftImage,
      rightImage,
      offset
    } = this.props as AnimatedImageButtonsProps & DefaultAnimatedImageButtonsProps;
    const aspectRatio: number = imageAspectRatio * 100 / imageWidthInPercents;

    return (
      <StyledAnimatedImageButtonsContainer
        innerRef={(container) => this.container = container}
        paddingTopInPercents={1 / aspectRatio * 100 - leftImageBottomOffsetInPercents}
        bottomOffset={offset}
      >
        <StyledAnimatedLeftImageButton
          image={leftImage}
          topOffset="0"
          imageWidthInPercents={imageWidthInPercents}
          translateInPercents={+this.state.translated * translateInPercents}
          transparency={!this.state.translated}
        />
        <StyledAnimatedRightImageButton
          image={rightImage}
          topOffset={`${rightImageTopOffsetInPercents / aspectRatio}%`}
          imageWidthInPercents={imageWidthInPercents}
          translateInPercents={+this.state.translated * translateInPercents * (-1)}
          transparency={!this.state.translated}
        />
      </StyledAnimatedImageButtonsContainer>
    );
  }

  private updateTranslated(): void {
    if (this.state.translated === false) {
      this.setState({translated: this.isFullyVisible()});
    }
  }

  private isFullyVisible(): boolean {
    return this.container
      && this.container.getBoundingClientRect().top >= 0
      && this.container.getBoundingClientRect().bottom <= window.innerHeight;
  }

}
