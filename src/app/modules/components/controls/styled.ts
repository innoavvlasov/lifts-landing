import { COLORS } from '../../constants';
import { css } from 'styled-components';

export const styledControlCss = css`
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  outline: 0;
  border-radius: 5px;
  cursor: pointer;
  transition: box-shadow 0.1s ease-in-out, color 0.1s ease-in-out;
  color: ${COLORS.WHITE};
  padding: 6px;

  &:hover,
  &:focus,
  &:active {
    box-shadow: 0 0 0 1px ${COLORS.GRAY};
    color: ${COLORS.GRAY};
  }
`;

export const styledAsceticControlCss = css`
  ${styledControlCss};
  border: 0;
  background-color: transparent;
`;
