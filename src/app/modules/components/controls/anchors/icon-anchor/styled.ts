import styled from 'styled-components';
import { styledAsceticControlCss } from '../../styled';

interface StyledIconAnchorProps {
  contentText: string;
}

export const StyledIconAnchor = styled<StyledIconAnchorProps, 'a'>('a')`
  ${styledAsceticControlCss};
  text-decoration: none;

  &::before {
    content: ${props => props.contentText};
    /* stylelint-disable-next-line font-family-no-missing-generic-family-keyword */
    font: normal normal 25px icomoon;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    speak: none;
  }
`;
