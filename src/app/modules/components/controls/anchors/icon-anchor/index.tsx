import * as React from 'react';
import { StyledIconAnchor } from './styled';

interface IconAnchorProps {
  contentText: string;
  name: string;
  href: string;
  className?: string;
}

export class IconAnchor extends React.PureComponent<IconAnchorProps, {}> {

  public render(): React.ReactNode {
    return (
      <StyledIconAnchor
        href={this.props.href}
        contentText={this.props.contentText}
        title={this.props.name}
        className={this.props.className}
      />
    );
  }

}
