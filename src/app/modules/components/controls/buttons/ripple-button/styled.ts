import styled, { keyframes } from 'styled-components';
import { styledControlCss } from '../../styled';
import { COLORS } from '../../../../constants';

const rippleAnimation = keyframes`
  to {
    opacity: 0;
    transform: scale(2);
  }
`;

export const StyledRippleButton = styled.button`
  ${styledControlCss};
  position: relative;
  overflow: hidden;
`;

export const StyledRippleContainer = styled.span`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

export interface StyledRippleProps {
  animationTimeInMs: number;
  ripplePositionX: string;
  ripplePositionY: string;
  rippleHeight: string;
  rippleWidth: string;
}

export const StyledRipple = styled<StyledRippleProps, 'span'>('span')`
  transform: scale(0);
  border-radius: 100%;
  position: absolute;
  opacity: 0.75;
  background-color: ${COLORS.WHITE};
  animation: ${rippleAnimation} ${props => props.animationTimeInMs}ms;
  left: ${props => props.ripplePositionX};
  top: ${props => props.ripplePositionY};
  height: ${props => props.rippleHeight};
  width: ${props => props.rippleWidth};
`;
