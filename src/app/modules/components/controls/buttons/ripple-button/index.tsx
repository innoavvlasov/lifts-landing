import * as React from 'react';
import { StyledRipple, StyledRippleButton, StyledRippleContainer, StyledRippleProps } from './styled';

interface RippleButtonProps {
  className?: string;
  text: string;
  action: () => void;
}

interface RippleButtonState {
  ripples: StyledRippleProps[];
  timerId: number | null;
}

export default class RippleButton extends React.PureComponent<RippleButtonProps, RippleButtonState> {

  private static readonly TIME_IN_MS: number = 750;

  private button: HTMLButtonElement;

  public constructor(props: RippleButtonProps) {
    super(props);
    this.state = {
      ripples: [],
      timerId: null
    };
  }

  public componentWillUnmount(): void {
    if (this.state.timerId) {
      window.clearTimeout(this.state.timerId);
    }
  }

  public render(): React.ReactNode {
    return (
      <StyledRippleButton
        className={this.props.className}
        onClick={(event) => {
          this.handleClick(event);
          this.props.action();
        }}
        innerRef={(button) => this.button = button}
        type="button"
      >
        {this.props.text}
        {(this.state.ripples.length > 0) && (
          <StyledRippleContainer>
            {this.state.ripples.map((rippleProps, index) => <StyledRipple key={index} {...rippleProps}/>)}
          </StyledRippleContainer>
        )}
      </StyledRippleButton>
    );
  }

  private handleClick(event: React.MouseEvent<HTMLButtonElement>) {
    const rippleProps = this.getRippleProps(event);
    const timerId: number =
      window.setTimeout(() => this.setState(() => ({ripples: []})), RippleButton.TIME_IN_MS);

    this.setState((previousState) => {
      if (previousState.timerId) {
        window.clearTimeout(previousState.timerId);
      }
      return {
        ripples: [...previousState.ripples, rippleProps],
        timerId
      };
    });
  }

  private getRippleProps(event: React.MouseEvent<HTMLButtonElement>): StyledRippleProps {
    const rect: ClientRect = this.button.getBoundingClientRect();
    const x: number = event.pageX === 0 ? rect.left + (this.button.offsetWidth / 2) : event.pageX;
    const y: number = event.pageY === 0 ? rect.top + (this.button.offsetHeight / 2) : event.pageY;
    const radius: number = Math.max(this.button.offsetWidth, this.button.offsetHeight);

    return {
      animationTimeInMs: RippleButton.TIME_IN_MS,
      ripplePositionX: `${(x - rect.left - (this.button.offsetWidth / 2))}px`,
      ripplePositionY: `${(y - rect.top - (this.button.offsetWidth / 2))}px`,
      rippleHeight: `${radius}px`,
      rippleWidth: `${radius}px`
    };
  }

}
