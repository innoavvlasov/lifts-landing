import * as React from 'react';
import { StyledTryNowButton } from './styled';
import { SendStateEventProps } from '../../../../index';

export default class TryNowButton extends React.PureComponent<SendStateEventProps, {}> {

  public render(): React.ReactNode {
    return (
      <StyledTryNowButton
        onClick={() => this.props.sendStateEvent('NEXT')}
        type="button"
      >
        Попробуй сейчас
      </StyledTryNowButton>
    );
  }

}
