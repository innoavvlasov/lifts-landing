import styled from 'styled-components';
import { COLORS, FONTS_STACK, LINEAR_GRADIENTS } from '../../../../constants';
import { styledControlCss } from '../../styled';

export const StyledTryNowButton = styled.button`
  ${styledControlCss};
  border: 1px solid ${COLORS.ORANGE};
  border-radius: 24px;
  background-image: ${LINEAR_GRADIENTS.YELLOW};
  font: normal 700 18px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
  padding: 16px 52px;

  &:hover,
  &:focus {
    border-color: ${COLORS.GRAY};
    box-shadow: 0 0 32px 0 rgba(53, 64, 82, 0.5);
  }

  &:active {
    box-shadow: 0 0 16px 0 rgba(53, 64, 82, 0.5);
  }
`;
