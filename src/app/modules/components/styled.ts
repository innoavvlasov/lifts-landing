import styled, { css } from 'styled-components';
import { WIDTH } from '../constants';

export const fullWidthFlexContainerCss = css`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const StyledHeading = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: baseline;
  width: 100%;
  max-width: ${WIDTH.PIXELS.LEVEL2}px;
`;

export const StyledHeadingRightBlock = styled.div`
  @media only screen and (max-width: 700px) {
    margin-left: auto;
  }
`;

/**
 * https://github.com/h5bp/html5-boilerplate/blob/76f6b7d95358ffabb6ac6c4bc8d44a5af35fea8d/src/css/main.css#L121-L141
 */
export const visuallyHidden = css`
  border: 0;
  clip: rect(0 0 0 0);
  clip-path: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
  white-space: nowrap;
`;
