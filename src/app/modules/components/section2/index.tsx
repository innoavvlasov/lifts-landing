import * as React from 'react';
import {
  StyledSection2Container,
  StyledSection2H1,
  StyledSection2H2,
  StyledSection2TextSequence,
  StyledSection2TextSequenceItem,
  StyledSection2TextSequenceItemDotted
} from './styled';

export default class Section2 extends React.PureComponent<{}, {}> {

  public render(): React.ReactNode {
    return (
      <StyledSection2Container>
        <StyledSection2H1>
          Система Mediapult объединила в себе всех владельцев рекламных поверхностей в лифтах Хабаровска.
        </StyledSection2H1>
        <StyledSection2H2>
          Сервис MediaPult:
        </StyledSection2H2>

        <StyledSection2TextSequence>
          <StyledSection2TextSequenceItem>
            Объединяет данные о всех предложениях и ценах на рекламу в лифтах
          </StyledSection2TextSequenceItem>
          <StyledSection2TextSequenceItemDotted>
            Показывает в рамках одного web окна всех поставщиков и их цены
          </StyledSection2TextSequenceItemDotted>
          <StyledSection2TextSequenceItemDotted>
            Сравнивает поставщиков по ценам, размеру аудитории и другим важным показателям
          </StyledSection2TextSequenceItemDotted>
          <StyledSection2TextSequenceItemDotted>
            Загружает в личный кабинет фотоотчет о прошедшей рекламной активности и прочие закрывающие документы
          </StyledSection2TextSequenceItemDotted>
        </StyledSection2TextSequence>
      </StyledSection2Container>
    );
  }

}
