import styled from 'styled-components';
import { COLORS, FONTS_STACK, LINEAR_GRADIENTS, WIDTH } from '../../constants';
import { fullWidthFlexContainerCss } from '../styled';

export const StyledSection2Container = styled.section`
  ${fullWidthFlexContainerCss};
  overflow: hidden;
  padding: 124px 0 101px;
  background-image: ${LINEAR_GRADIENTS.BLUE};
`;

const StyledSection2Heading = styled.h1`
  margin: 0 auto;
  text-align: center;
  width: ${WIDTH.PERCENTS.LEVEL1}%;
  max-width: ${WIDTH.PIXELS.LEVEL3}px;
  color: ${COLORS.WHITE};
`;

export const StyledSection2H1 = StyledSection2Heading.extend`
  font: normal 400 32px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
`;

export const StyledSection2H2 = StyledSection2Heading.extend`
  font: normal 600 32px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
`.withComponent('h2');

const itemsOffset: string = '20px';
const mediaThreshold: string = '950px';

export const StyledSection2TextSequence = styled.div`
  width: ${WIDTH.PERCENTS.LEVEL1}%;
  max-width: ${WIDTH.PIXELS.LEVEL2}px;
  margin: 52px -${itemsOffset} 0;
  counter-reset: index;
  display: flex;

  @media only screen and (max-width: ${mediaThreshold}) {
    flex-direction: column;
    align-items: center;
    margin-left: 0;
    margin-right: 0;
  }
`;

export const StyledSection2TextSequenceItem = styled.p`
  margin: 0;
  padding-left: ${itemsOffset};
  padding-right: ${itemsOffset};
  width: 100%;
  text-align: center;
  padding-top: 81px;
  font: normal 400 18px/1.662 ${FONTS_STACK.PROXIMA_NOVA};
  color: ${COLORS.WHITE};
  position: relative;

  &::before {
    counter-increment: index;
    content: counter(index);
    text-align: center;
    font: normal 400 32px/64px ${FONTS_STACK.PROXIMA_NOVA};
    color: ${COLORS.WHITE};
    box-sizing: border-box;
    width: 64px;
    height: 64px;
    border: 2px solid ${COLORS.WHITE};
    border-radius: 50%;
    position: absolute;
    display: block;
    left: 0;
    right: 0;
    top: 0;
    margin: 0 auto 17px;
  }

  @media only screen and (max-width: ${mediaThreshold}) {
    width: ${WIDTH.PERCENTS.LEVEL2}%;
    display: flex;
    padding-left: 0;
    padding-right: 0;
    align-items: stretch;
    flex-direction: column;
    padding-top: 15px;

    &::before {
      position: static;
    }
  }
`;

export const StyledSection2TextSequenceItemDotted = StyledSection2TextSequenceItem.extend`
  &::after {
    content: '';
    background-color: ${COLORS.WHITE};
    width: 8px;
    height: 8px;
    border-radius: 50%;
    position: absolute;
    display: block;
    left: 0;
    right: 0;
    top: 28px;
    margin-right: 100%;
    transform: translate3d(-43px, 0, 0);
    box-shadow: 26px 0 0 0 ${COLORS.WHITE}, 52px 0 0 0 ${COLORS.WHITE}, 78px 0 0 0 ${COLORS.WHITE};
  }

  @media only screen and (max-width: ${mediaThreshold}) {
    &::after {
      position: static;
      order: -1;
      margin: 0 0 102px;
      box-shadow: 0 26px 0 0 ${COLORS.WHITE}, 0 52px 0 0 ${COLORS.WHITE}, 0 78px 0 0 ${COLORS.WHITE};
      transform: none;
      align-self: center;
    }
  }
`;
