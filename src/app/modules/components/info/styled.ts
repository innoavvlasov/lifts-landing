import styled from 'styled-components';
import { FONTS_STACK } from '../../constants';
import { styledAsceticControlCss } from '../controls/styled';

export const StyledInfoButton = styled.button`
  ${styledAsceticControlCss};
`;

export const StyledInfoMiddleButton = styled.button`
  ${styledAsceticControlCss};
  margin: 0 24px;
`;

export const StyledInfoContainer = styled.nav`
  font: normal 600 14px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
  text-align: center;

  @media only screen and (max-width: 700px) {
    order: 1;
    flex-basis: 100%;
    margin-top: 14px;
  }
`;
