import * as React from 'react';
import { StyledInfoButton, StyledInfoContainer, StyledInfoMiddleButton } from './styled';

export default class Info extends React.PureComponent<{}, {}> {

  public render(): React.ReactNode {
    return (
      <StyledInfoContainer>
        <StyledInfoButton type="button">Носители</StyledInfoButton>
        <StyledInfoMiddleButton type="button">Сервис</StyledInfoMiddleButton>
        <StyledInfoButton type="button">Контакты</StyledInfoButton>
      </StyledInfoContainer>
    );
  }

}
