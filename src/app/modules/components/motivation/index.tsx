import * as React from 'react';
import {
  StyledMotivationContainer,
  StyledMotivationContent,
  StyledMotivationHeader,
  StyledMotivationItem,
  StyledMotivationItemDescription,
  StyledMotivationItemTitle
} from './styled';

export default class Motivation extends React.PureComponent<{}, {}> {

  public render(): React.ReactNode {
    return (
      <StyledMotivationContainer>
        <StyledMotivationHeader>Почему реклама в лифтах?</StyledMotivationHeader>

        <StyledMotivationContent>
          <StyledMotivationItem imageFileName="1">
            <StyledMotivationItemTitle>Заметно сразу</StyledMotivationItemTitle>
            <StyledMotivationItemDescription>
              Каждый день люди используют лифт своего жилого дома, они обязательно внимательно изучат
              предложения, расположенные в лифте.
            </StyledMotivationItemDescription>
          </StyledMotivationItem>

          <StyledMotivationItem imageFileName="2">
            <StyledMotivationItemTitle>Опыт, проверенный многими</StyledMotivationItemTitle>
            <StyledMotivationItemDescription>
              Компании давно убедились в эффективности такого продвижения и уже выбрали основным
              инструментом продвижения рекламу в лифтах.
            </StyledMotivationItemDescription>
          </StyledMotivationItem>

          <StyledMotivationItem imageFileName="3">
            <StyledMotivationItemTitle>Эффективная реклама</StyledMotivationItemTitle>
            <StyledMotivationItemDescription>
              При сравнительно невысоких затратах увеличить продажи вашей компании и обойти
              конкурентов, занимающихся той же деятельностью в определённом районе города.
            </StyledMotivationItemDescription>
          </StyledMotivationItem>

          <StyledMotivationItem imageFileName="4">
            <StyledMotivationItemTitle>Видят все</StyledMotivationItemTitle>
            <StyledMotivationItemDescription>
              Каждый день, контактируя с вашей рекламной информацией, люди рано или поздно обратятся
              именно к вам.
            </StyledMotivationItemDescription>
          </StyledMotivationItem>

          <StyledMotivationItem imageFileName="5">
            <StyledMotivationItemTitle>Легко запомнить</StyledMotivationItemTitle>
            <StyledMotivationItemDescription>
              Человек видит сообщение на одном стенде в среднем от 60 до 94 раз в месяц.
            </StyledMotivationItemDescription>
          </StyledMotivationItem>
        </StyledMotivationContent>
      </StyledMotivationContainer>
    );
  }

}
