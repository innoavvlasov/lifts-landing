import styled from 'styled-components';
import { COLORS, FONTS_STACK, WIDTH } from '../../constants';

export const StyledMotivationContainer = styled.article`
  max-width: ${WIDTH.PIXELS.LEVEL2}px;
  width: ${WIDTH.PERCENTS.LEVEL1}%;
  margin: 88px auto 93px;
  overflow: hidden;
`;

export const StyledMotivationHeader = styled.h1`
  margin: 0;
  color: ${COLORS.GRAY};
  font: normal 300 32px/1 ${FONTS_STACK.PROXIMA_NOVA};
  text-align: center;
`;

const itemsOffset: string = '28px';

export const StyledMotivationContent = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding-top: 38px;
  margin: 0 -${itemsOffset};
`;

function initializeImageUrl(fileName: string): string {
  const image = require(`./${fileName}.svg`);
  return `url(${image}) no-repeat top center/133px auto;`;
}

interface StyledMotivationItemProps {
  imageFileName: string;
}

export const StyledMotivationItem = styled<StyledMotivationItemProps, 'div'>('div')`
  background: ${props => initializeImageUrl(props.imageFileName)};
  margin-bottom: 26px;
  flex: 0 1 345px;
  padding: 0 ${itemsOffset};

  @media only screen and (max-width: 1200px) {
    flex-grow: 1;
  }
`;

export const StyledMotivationItemTitle = styled.h1`
  margin: 0;
  text-align: center;
  font: normal 400 32px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
  color: ${COLORS.BLUE};
  padding-top: 131px;
`;

export const StyledMotivationItemDescription = styled.p`
  text-align: center;
  margin: 4px 0 0;
  font: normal 400 18px/1.333 ${FONTS_STACK.PROXIMA_NOVA};
  color: ${COLORS.GRAY};
`;
