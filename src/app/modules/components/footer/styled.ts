import styled from 'styled-components';
import { fullWidthFlexContainerCss, StyledHeading, StyledHeadingRightBlock } from '../styled';
import { COLORS, FONTS_STACK, LINEAR_GRADIENTS, WIDTH } from '../../constants';
import { IconAnchor } from '../controls/anchors/icon-anchor';

export const StyledFooterContainer = styled.footer`
  ${fullWidthFlexContainerCss};
  background-image: ${LINEAR_GRADIENTS.BLUE};
  overflow: hidden;
  padding-bottom: 36px;
`;

export const StyledFooterHeading = StyledHeading.extend`
  padding-top: 30px;
`;

export const StyledFooterText = StyledHeadingRightBlock.extend`
  margin-right: 4px;
  font: normal 400 24px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
  color: ${COLORS.WHITE};
`;

const offset: string = '25px';

export const NavIconAnchor = styled(IconAnchor)`
  padding-left: ${offset};
  padding-right: ${offset};
  margin-top: 26px;
`;

export const StyledButtonsBlock = styled.nav`
  width: ${WIDTH.PERCENTS.LEVEL2}%;
  max-width: 600px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  margin: 0 -${offset};
`;
