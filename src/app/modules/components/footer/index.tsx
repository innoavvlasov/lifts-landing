import * as React from 'react';
import Logo from '../logo';
import Info from '../info';
import {
  NavIconAnchor,
  StyledButtonsBlock,
  StyledFooterContainer,
  StyledFooterHeading,
  StyledFooterText
} from './styled';
import { ICONS } from '../../constants';
import { OpenAppProps } from '../../index';

export default class Footer extends React.PureComponent<OpenAppProps, {}> {

  public render(): React.ReactNode {
    return (
      <StyledFooterContainer>
        <StyledFooterHeading>
          <Logo {...this.props}/>
          <Info/>
          <StyledFooterText>+78008886677</StyledFooterText>
        </StyledFooterHeading>
        <StyledButtonsBlock>
          <NavIconAnchor
            contentText={`"\\${ICONS.Vk.symbol}"`}
            name={ICONS.Vk.name}
            href={ICONS.Vk.href}
          />
          <NavIconAnchor
            contentText={`"\\${ICONS.Twitter.symbol}"`}
            name={ICONS.Twitter.name}
            href={ICONS.Twitter.href}
          />
          <NavIconAnchor
            contentText={`"\\${ICONS.YouTube.symbol}"`}
            name={ICONS.YouTube.name}
            href={ICONS.YouTube.href}
          />
          <NavIconAnchor
            contentText={`"\\${ICONS.Facebook.symbol}"`}
            name={ICONS.Facebook.name}
            href={ICONS.Facebook.href}
          />
          <NavIconAnchor
            contentText={`"\\${ICONS.GooglePlus.symbol}"`}
            name={ICONS.GooglePlus.name}
            href={ICONS.GooglePlus.href}
          />
          <NavIconAnchor
            contentText={`"\\${ICONS.Instagram.symbol}"`}
            name={ICONS.Instagram.name}
            href={ICONS.Instagram.href}
          />
          <NavIconAnchor
            contentText={`"\\${ICONS.Linkedin.symbol}"`}
            name={ICONS.Linkedin.name}
            href={ICONS.Linkedin.href}
          />
          <NavIconAnchor
            contentText={`"\\${ICONS.Odnoklassniki.symbol}"`}
            name={ICONS.Odnoklassniki.name}
            href={ICONS.Odnoklassniki.href}
          />
        </StyledButtonsBlock>
      </StyledFooterContainer>
    );
  }

}
