import styled from 'styled-components';
import { COLORS, FONTS_STACK, WIDTH } from '../../constants';
import { StyledAnimatedImageButtonsContainer } from '../animated-image-buttons/styled';

/* https://github.com/styled-components/stylelint-processor-styled-components/issues/54 */
export const StyledSection1Container = styled.section`
  @media only screen and (max-width: 1200px) {
    margin-bottom: 50px;

    /* stylelint-disable declaration-empty-line-before, declaration-block-semicolon-newline-after */
    /* stylelint-disable rule-empty-line-before */
    ${/* sc-selector */ StyledAnimatedImageButtonsContainer} {
      display: none;
    }
    /* stylelint-enable */
  }
`;

export const StyledSection1H1 = styled.h1`
  text-align: center;
  width: ${WIDTH.PERCENTS.LEVEL1}%;
  margin: 82px auto 0;
  max-width: ${WIDTH.PIXELS.LEVEL3}px;
  font: normal 400 32px/1.2 ${FONTS_STACK.PROXIMA_NOVA};
  color: ${COLORS.BLUE};
`;

export const StyledSection1P = styled.p`
  text-align: center;
  width: ${WIDTH.PERCENTS.LEVEL1}%;
  max-width: 815px;
  font: normal 400 18px/1.662 ${FONTS_STACK.PROXIMA_NOVA};
  color: ${COLORS.GRAY};
  margin: 26px auto 50px;
`;
