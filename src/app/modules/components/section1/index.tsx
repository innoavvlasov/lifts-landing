import * as React from 'react';
import { isWebpSupported } from '../../featureDetection';
import AnimatedImageButtons from '../animated-image-buttons';
import { StyledSection1Container, StyledSection1H1, StyledSection1P } from './styled';

export default class Section1 extends React.PureComponent<{}, {}> {

  public render(): React.ReactNode {
    return (
      <StyledSection1Container>
        <StyledSection1H1>
          Mediapult в режиме реального времени рассчитает стоимость рекламы в лифтах Хабаровска.
        </StyledSection1H1>
        <StyledSection1P>
          Система самостоятельно выставит вам счет на оплату, предоставит все закрывающие документы, а также
          загрузит фотоотчет в ваш личный кабинет в системе.
        </StyledSection1P>
        <AnimatedImageButtons
          imageAspectRatio={1230 / 1106}
          leftImage={isWebpSupported() ? require('./left.webp') : require('./left.jpg')}
          rightImage={isWebpSupported() ? require('./right.webp') : require('./right.jpg')}
        />
      </StyledSection1Container>
    );
  }

}
