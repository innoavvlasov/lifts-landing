import * as React from 'react';
import Logo from '../logo';
import Info from '../info';
import TryNowButton from '../controls/buttons/try-now-button';
import { StyledHeading } from '../styled';
import { StyledHeaderContainer, StyledHeaderDiv, StyledHeaderP } from './styled';
import { isWebpSupported } from '../../featureDetection';
import Identification from '../identification';
import { OpenAppProps, SendStateEventProps } from '../../index';

interface HeaderState {
  backgroundProperty: string;
}

export default class Header extends React.PureComponent<OpenAppProps & SendStateEventProps, HeaderState> {

  public constructor(props: OpenAppProps & SendStateEventProps) {
    super(props);
    const image = isWebpSupported() ? require(`./image.webp`) : require(`./image.jpg`);
    this.state = {
      backgroundProperty: `url(${image}) no-repeat center center / 100% 100%;`
    };
  }

  public render(): React.ReactNode {
    return (
      <StyledHeaderContainer backgroundProperty={this.state.backgroundProperty}>
        <StyledHeading>
          <Logo {...this.props} />
          <Info/>
          <Identification action={() => this.props.openApp('identification.bundle.js')}/>
        </StyledHeading>
        <StyledHeaderP>
          Mediapult позволит вам разместить рекламу в лифтах вашего города в режиме онлайн.
        </StyledHeaderP>
        <StyledHeaderDiv>
          <TryNowButton sendStateEvent={this.props.sendStateEvent}/>
        </StyledHeaderDiv>
      </StyledHeaderContainer>
    );
  }

}
