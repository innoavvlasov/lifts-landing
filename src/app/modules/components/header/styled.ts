import { fullWidthFlexContainerCss, StyledHeading } from '../styled';
import styled from 'styled-components';
import { COLORS, FONTS_STACK, WIDTH } from '../../constants';

interface StyledHeaderContainerProps {
  backgroundProperty: string;
}

/* https://github.com/styled-components/stylelint-processor-styled-components/issues/54 */
export const StyledHeaderContainer = styled<StyledHeaderContainerProps, 'header'>('header')`
  ${fullWidthFlexContainerCss};
  min-height: 617px;
  background: ${props => props.backgroundProperty};

  /* stylelint-disable rule-empty-line-before, declaration-block-semicolon-newline-after */
  /* stylelint-disable no-duplicate-selectors, declaration-empty-line-before */
  ${/* sc-selector */ StyledHeading} {
    padding-top: 14px;
  }
  /* stylelint-enable */

  @media only screen and (max-width: ${WIDTH.PIXELS.LEVEL1}px) {
    background-size: auto 100%;
  }
`;

export const StyledHeaderP = styled.p`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  flex: 100 1 auto;
  margin: 0;
  text-align: center;
  width: ${WIDTH.PERCENTS.LEVEL2}%;
  max-width: 780px;
  font: normal 300 36px/1 ${FONTS_STACK.PROXIMA_NOVA};
  word-spacing: 6px;
  color: ${COLORS.WHITE};
`;

export const StyledHeaderDiv = styled.div`
  flex: 114 1 auto;
  padding-top: 41px;
`;
