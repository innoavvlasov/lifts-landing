import * as React from 'react';
import Header from './components/header';
import Motivation from './components/motivation';
import Section1 from './components/section1';
import Section2 from './components/section2';
import Section3 from './components/section3';
import Footer from './components/footer';
import Section4 from './components/section4';
import { StyledDelimiter } from './styled';

export interface OpenAppProps {
  openApp: (name: string) => void;
}

export interface SendStateEventProps {
  sendStateEvent: (name: string) => void;
}

interface LandingProps extends OpenAppProps, SendStateEventProps {}

export default class Landing extends React.PureComponent<LandingProps, any> {

  public render(): JSX.Element {
    return (
      <React.Fragment>
        <Header {...this.props}/>
        <Motivation/>
        <StyledDelimiter/>
        <Section1/>
        <Section2/>
        <Section3/>
        <Section4 sendStateEvent={this.props.sendStateEvent}/>
        <Footer openApp={this.props.openApp}/>
      </React.Fragment>
    );
  }
}
