import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { injectGlobal } from 'styled-components';

import App from '../modules';

injectGlobal`
  * {
    margin: 0;
    padding: 0;
    font-family: 'Open Sans', sans-serif;
  }

  html {
    overflow-x: hidden;
  }
`;

const mocksSends = {
  openApp: (s: string) => console.info(s),
  sendStateEvent: this.openApp
};

ReactDOM.render(<App {...mocksSends}/>, document.getElementById('app') as HTMLElement);
