const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
		mode: 'production',
    entry: {
        lift: "./src/rm/index.tsx"
    },    
    output: {
        filename: "../../public/[name].bundle.js",
        path: path.resolve(__dirname, "dist"),
        libraryTarget: 'amd'
    },
    node: {
        fs: "empty"
    },
    plugins: [
			  new HtmlWebpackPlugin({
			  	title: 'Реклама в лифтах',
			  	template: './index.html',
			  	filename: 'lift-landing.html'
			  }),
		],

    resolve: {
        modules: ['node_modules', 'src'],
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"]
    },

    module: {
        rules: [
            { test: /\.tsx?$/, use: "awesome-typescript-loader" },
            { test: /\.(jpe?g|gif|png|svg|woff2?|ttf|eot|wav|mp3|webp)$/, use: "file-loader" }
        ],

    },
    externals:{
        "react": "react",
        "react-dom": "react-dom"
    }
};